import React, { useContext, useState, useEffect, useLayoutEffect } from 'react';
import { NavLink } from "react-router-dom";
import AuthAPI from "../services/AuthAPI";
import AuthContext from "../contexts/AuthContext";
import {toast} from "react-toastify";

const Navbar = ({ history }) => {

	const { isAuthenticated, setIsAuthenticated } = useContext(AuthContext);

	const [isOpen, setIsOpen] = useState(false);

	const [responsiveMode, setResponsiveMode] = useState(false);

	const [size, setSize] = useState([0,0]);

	useLayoutEffect(() => {
		function updateSize() {
			setSize([window.innerWidth, window.innerHeight]);
		}
		if (size[0] < 1024) {
			setResponsiveMode(true);
		} else {
			setResponsiveMode(false);
		}
		window.addEventListener('resize', updateSize);
		updateSize();
		return () => window.removeEventListener('resize', updateSize);
	}, []);

	const handleLogout = () => {
		AuthAPI.logout();
		setIsAuthenticated(false);
		toast.info("Vous êtes désormais déconnecté !");
		history.push("/login");
	};

	const toggleNavMenu = () => {
		setIsOpen(!isOpen);
		console.log(isOpen);
	};

	return (
		<nav className="navbar navbar-expand-lg navbar-light bg-light">
			<NavLink className="navbar-brand" to="/">SymReact</NavLink>
			{responsiveMode &&
			<button
				className="navbar-toggler"
				type="button"
				data-toggle="collapse"
				aria-label="Toggle navigation"
				onClick={toggleNavMenu}
			>
				<span className="navbar-toggler-icon"></span>
			</button>}

			<div className={`collapse navbar-collapse ${(responsiveMode && isOpen) && "show"}`}>
				<ul className="navbar-nav mr-auto">
					<li className="nav-item">
						<NavLink className="nav-link" to="/customers">Clients</NavLink>
					</li>
					<li className="nav-item">
						<NavLink className="nav-link" to="/invoices">Factures</NavLink>
					</li>
				</ul>
				<ul className="navbar-nav ml-auto">
					{!isAuthenticated ? <>
						<li className="nav-item">
							<NavLink to="/register" className="btn btn-info">Inscription</NavLink>
						</li>
						<li className="nav-item">
							<NavLink to="/login" className="btn btn-success">Connexion</NavLink>
						</li>
					</> : <li className="nav-item">
						<button onClick={handleLogout} className="btn btn-danger">Déconnexion</button>
					</li>}
				</ul>
			</div>
		</nav>
	);
};

export default Navbar;
